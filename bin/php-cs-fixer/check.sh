#!/bin/sh

set -eu

vendor/bin/php-cs-fixer fix --dry-run --diff "$@"
