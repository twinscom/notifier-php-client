#!/bin/sh

set -eu

SCRIPTS_DIR=$(dirname "$0")

"$SCRIPTS_DIR/php-cs-fixer/check.sh"
"$SCRIPTS_DIR/phpmd.sh"
"$SCRIPTS_DIR/phpunit.sh" "$@"
