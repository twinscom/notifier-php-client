<?php

declare(strict_types=1);

namespace twinscom\Notifier;

use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use twinscom\GuzzleComponents\BackoffRetryDelay;
use twinscom\GuzzleComponents\RetryDecider;

class Client
{
    /**
     * @var array
     */
    private $params;

    /**
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    public function __construct(array $params)
    {
        $handlerStack = HandlerStack::create();

        $handlerStack->push(
            Middleware::retry(
                RetryDecider::make($params['maxRetries'] ?? 2),
                BackoffRetryDelay::make(
                    $params['retryDelayCoefficient'] ?? 300
                )
            ),
            'Middleware::retry'
        );

        $this->params = array_key_exists('guzzleHttpClient', $params)
            ? $params
            : array_replace(
                $params,
                [
                    'guzzleHttpClient' => new \GuzzleHttp\Client([
                        'base_uri' => $params['baseUri'],
                        'handler' => $handlerStack,
                    ]),
                ]
            );
    }

    public function sendEmail(array $params): void
    {
        $this->send($params, '/email');
    }

    public function sendPush(array $params): void
    {
        $this->send($params, '/push');
    }

    private function send(array $params, string $path): void
    {
        $guzzleHttpClient = $this->params['guzzleHttpClient'];

        assert($guzzleHttpClient instanceof \GuzzleHttp\Client);

        $callOnErrorIfExists = static function (string $message) use ($params): void {
            if (array_key_exists('onError', $params)) {
                $params['onError']($message);
            }
        };

        try {
            $guzzleHttpClient->post($path, [
                'query' => array_replace(
                    ['template-id' => $params['templateId']],
                    ['api-key' => $this->params['apiKey']]
                ),
                'json' => $params['message'],
            ]);
        } catch (RequestException $error) {
            $message = $error->hasResponse()
                ? json_decode(
                    $error->getResponse()->getBody()->getContents(),
                    true
                )['message']
                : $error->getMessage();

            $callOnErrorIfExists($message);

            return;
        } catch (\Exception $error) {
            $message = $error->getMessage();

            $callOnErrorIfExists($message);

            return;
        }

        if (array_key_exists('onSuccess', $params)) {
            $params['onSuccess']();
        }
    }
}
