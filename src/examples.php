<?php

declare(strict_types=1);

use twinscom\Notifier\Client;

require dirname(__DIR__) . '/vendor/autoload.php';

$notifierClient = new Client([
    'baseUri' => 'http://localhost/',
    'apiKey' => '123',
]);

$notifierClient->sendEmail([
    'templateId' => 'welcome',

    'message' => [
        'recipients' => [
            [
                'to' => 'username@example.com',
                'locale' => 'en',
                'delay' => 0,
            ],
            [
                'to' => 'another-username@example.com',
                'locale' => 'ru',

                // one minute
                'delay' => 60,
            ],
        ],

        // data for the template
        'data' => [
            'greeting' => 'Hello',
        ],

        // localized data for the template
        'localizedData' => [
            'ru' => [
                'greeting' => 'Привет',
            ],
        ],

        'from' => 'sender@example.com',

        'options' => [
            'name' => 'Sender Name',
            'replyTo' => 'reply-to@example.com',
        ],
    ],

    'onSuccess' => static function (): void {
        echo "Email accepted!\n";
    },

    'onError' => static function ($message): void {
        echo "Email not accepted: ${message}\n";
    },
]);

$notifierClient->sendPush([
    'templateId' => 'welcome',

    'message' => [
        'recipients' => [
            [
                'to' => 'some-firebase-topic',
                'locale' => 'en',
                'delay' => 0,
            ],
            [
                'to' => 'another-firebase-topic',
                'locale' => 'ru',

                // one minute
                'delay' => 60,
            ],
        ],

        // data for the template
        'data' => [
            'greeting' => 'Hello',
        ],

        // localized data for the template
        'localizedData' => [
            'ru' => [
                'greeting' => 'Привет',
            ],
        ],

        'options' => [
            // Firebase notification payload
            'payload' => [
                'key' => 'value',
            ],
        ],
    ],

    'onSuccess' => static function (): void {
        echo "Push accepted!\n";
    },

    'onError' => static function ($message): void {
        echo "Push not accepted: ${message}\n";
    },
]);
